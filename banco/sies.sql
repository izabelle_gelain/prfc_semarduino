-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 19-Maio-2017 às 14:59
-- Versão do servidor: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sies`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acontecem`
--

CREATE TABLE `acontecem` (
  `id_leitura` int(11) NOT NULL,
  `id_cultivo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `bombas`
--

CREATE TABLE `bombas` (
  `id_bomba` int(11) NOT NULL,
  `cod_usuario` int(11) NOT NULL,
  `nome_bomba` varchar(200) COLLATE utf8_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Extraindo dados da tabela `bombas`
--

INSERT INTO `bombas` (`id_bomba`, `cod_usuario`, `nome_bomba`) VALUES
(7, 15, '12v'),
(8, 17, 'bomba_1'),
(9, 17, 'bomba_2'),
(10, 18, 'bomba_3'),
(11, 18, 'bomba_4'),
(12, 19, 'bomba_5'),
(13, 19, 'bomba_6'),
(14, 20, 'bomba_7'),
(15, 20, 'bomba_8'),
(16, 21, 'bomba_9'),
(17, 21, 'bomba_10');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cultivo`
--

CREATE TABLE `cultivo` (
  `id_cultivo` int(11) NOT NULL,
  `cod_usuario` int(11) NOT NULL,
  `cod_solo` int(11) NOT NULL,
  `cod_cultura` int(11) NOT NULL,
  `nome_cultivo` varchar(150) COLLATE utf8_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Extraindo dados da tabela `cultivo`
--

INSERT INTO `cultivo` (`id_cultivo`, `cod_usuario`, `cod_solo`, `cod_cultura`, `nome_cultivo`) VALUES
(21, 15, 6, 21, 'macieira'),
(22, 17, 7, 22, 'Cultivo do leo'),
(23, 17, 8, 23, 'cultivo de cana'),
(24, 18, 9, 24, 'cultivo de trigo'),
(25, 18, 10, 25, 'cultivo de milho'),
(26, 19, 11, 26, 'cultivo de soja'),
(27, 19, 12, 27, 'cultivo de tomilho'),
(28, 20, 13, 28, 'cultivo de cenoura'),
(29, 20, 14, 29, 'cultivo de carambola'),
(30, 21, 15, 30, 'cultivo de morango'),
(31, 21, 16, 31, 'cultivo de arroz');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cultura`
--

CREATE TABLE `cultura` (
  `id_cultura` int(11) NOT NULL,
  `cod_usuario` int(11) NOT NULL,
  `tipo_planta` varchar(20) COLLATE utf8_unicode_520_ci NOT NULL,
  `status_cult` varchar(20) COLLATE utf8_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Extraindo dados da tabela `cultura`
--

INSERT INTO `cultura` (`id_cultura`, `cod_usuario`, `tipo_planta`, `status_cult`) VALUES
(21, 15, 'maçã', 'Desativado'),
(22, 17, 'Alface', 'Desativado'),
(23, 17, 'cana de açúcar', 'Desativado'),
(24, 18, 'trigo', 'Desativado'),
(25, 18, 'milho', 'Desativado'),
(26, 19, 'soja', 'Desativado'),
(27, 19, 'tomilho de jardim', 'Desativado'),
(28, 20, 'cenoura', 'Desativado'),
(29, 20, 'carambola', 'Desativado'),
(30, 21, 'morango', 'Desativado'),
(31, 21, 'arroz', 'Desativado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `leituras`
--

CREATE TABLE `leituras` (
  `id_leitura` int(11) NOT NULL,
  `temperatura` varchar(15) COLLATE utf8_unicode_520_ci NOT NULL,
  `umidade_solo1` varchar(15) COLLATE utf8_unicode_520_ci NOT NULL,
  `umidade_solo2` varchar(20) COLLATE utf8_unicode_520_ci NOT NULL,
  `umidade_solo3` varchar(20) COLLATE utf8_unicode_520_ci NOT NULL,
  `umidade_ar` varchar(15) COLLATE utf8_unicode_520_ci NOT NULL,
  `data_leitura` date NOT NULL,
  `cod_bomba` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Extraindo dados da tabela `leituras`
--

INSERT INTO `leituras` (`id_leitura`, `temperatura`, `umidade_solo1`, `umidade_solo2`, `umidade_solo3`, `umidade_ar`, `data_leitura`, `cod_bomba`) VALUES
(3, '25', '111', '222', '333', '100', '2017-05-10', 7),
(4, '25', '1000', '144', '133', '11111', '2017-05-12', 7),
(5, '30', '87', '78', '94', '45', '2017-05-16', 8),
(6, '28', '85', '75', '30', '84', '2017-05-16', 9),
(7, '37', '54', '73', '78', '94', '2017-05-16', 10),
(8, '18', '37', '29', '47', '39', '2017-05-16', 11),
(9, '12', '76', '25', '84', '29', '2017-05-16', 12),
(10, '-10', '59', '38', '48', '72', '2017-05-16', 13),
(11, '22', '38', '47', '16', '40', '2017-05-16', 14),
(12, '33', '84', '36', '95', '37', '2017-05-16', 15),
(13, '08', '56', '47', '82', '16', '2017-05-16', 16),
(14, '34', '35', '67', '54', '24', '2017-05-16', 17);

-- --------------------------------------------------------

--
-- Estrutura da tabela `regadas`
--

CREATE TABLE `regadas` (
  `id_regadas` int(11) NOT NULL,
  `cod_bomba` int(11) NOT NULL,
  `cod_cultivo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Extraindo dados da tabela `regadas`
--

INSERT INTO `regadas` (`id_regadas`, `cod_bomba`, `cod_cultivo`) VALUES
(1, 8, 22),
(2, 9, 23),
(3, 10, 24),
(4, 11, 25),
(5, 12, 26),
(6, 13, 27),
(7, 14, 28),
(8, 15, 29),
(9, 16, 30),
(10, 17, 31);

-- --------------------------------------------------------

--
-- Estrutura da tabela `solo`
--

CREATE TABLE `solo` (
  `id_solo` int(11) NOT NULL,
  `cod_usuario` int(11) NOT NULL,
  `cap_de_campo` varchar(15) COLLATE utf8_unicode_520_ci NOT NULL,
  `status_solo` varchar(20) COLLATE utf8_unicode_520_ci NOT NULL,
  `tipo_solo` varchar(20) COLLATE utf8_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Extraindo dados da tabela `solo`
--

INSERT INTO `solo` (`id_solo`, `cod_usuario`, `cap_de_campo`, `status_solo`, `tipo_solo`) VALUES
(6, 15, '200', 'Desativado', 'substrato'),
(7, 17, '20kpa', 'Desativado', 'arenoso'),
(8, 17, '49kpa', 'Desativado', 'argiloso'),
(9, 18, '37kpa', 'Desativado', 'humoso'),
(10, 18, '29kpa', 'Desativado', 'calc?rio'),
(11, 19, '17kpa', 'Desativado', 'terra roxa'),
(12, 19, '8kpa', 'Desativado', 'seco'),
(13, 20, '27kpa', 'Desativado', 'fertilizado'),
(14, 20, '36kpa', 'Desativado', 'drenado'),
(15, 21, '67kpa', 'Desativado', 'franco'),
(16, 21, '87kpa', 'Desativado', 'encharcado');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipo_user`
--

CREATE TABLE `tipo_user` (
  `tipo_user` int(11) NOT NULL,
  `descricao` varchar(40) COLLATE utf8_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Extraindo dados da tabela `tipo_user`
--

INSERT INTO `tipo_user` (`tipo_user`, `descricao`) VALUES
(1, 'admin'),
(2, 'comum');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `senha` varchar(200) COLLATE utf8_unicode_520_ci NOT NULL,
  `email` varchar(40) COLLATE utf8_unicode_520_ci NOT NULL,
  `usuario` varchar(15) COLLATE utf8_unicode_520_ci NOT NULL,
  `nome` varchar(45) COLLATE utf8_unicode_520_ci NOT NULL,
  `cod_tipo_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `senha`, `email`, `usuario`, `nome`, `cod_tipo_user`) VALUES
(15, '202cb962ac59075b964b07152d234b70', 'belle@com.br', 'iza_gelain', 'Izabelle Maria Gelain', 2),
(17, 'e10adc3949ba59abbe56e057f20f883e', 'leleco_zinho@hotmail.com', 'leonardo', 'Leonardo Vitor', 2),
(18, '827ccb0eea8a706c4c34a16891f84e7b', 'joao@gmail.com', 'joao', 'JoÃ£ozinho', 2),
(19, '827ccb0eea8a706c4c34a16891f84e7b', 'nicolas@gmail.com', 'nicolas', 'Nicolas Santos', 2),
(20, '827ccb0eea8a706c4c34a16891f84e7b', 'amanda@gmail.com', 'amanda', 'Amanda', 2),
(21, '827ccb0eea8a706c4c34a16891f84e7b', 'tambosi@gmail.com', 'gabriel', 'gabriel tambosi', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acontecem`
--
ALTER TABLE `acontecem`
  ADD KEY `id_leitura` (`id_leitura`),
  ADD KEY `id_cultivo` (`id_cultivo`);

--
-- Indexes for table `bombas`
--
ALTER TABLE `bombas`
  ADD PRIMARY KEY (`id_bomba`),
  ADD KEY `id_usuario` (`cod_usuario`);

--
-- Indexes for table `cultivo`
--
ALTER TABLE `cultivo`
  ADD PRIMARY KEY (`id_cultivo`),
  ADD KEY `id_usuario` (`cod_usuario`),
  ADD KEY `id_solo` (`cod_solo`),
  ADD KEY `id_cultura` (`cod_cultura`);

--
-- Indexes for table `cultura`
--
ALTER TABLE `cultura`
  ADD PRIMARY KEY (`id_cultura`),
  ADD KEY `id_usuario` (`cod_usuario`);

--
-- Indexes for table `leituras`
--
ALTER TABLE `leituras`
  ADD PRIMARY KEY (`id_leitura`),
  ADD KEY `id_bomba` (`cod_bomba`);

--
-- Indexes for table `regadas`
--
ALTER TABLE `regadas`
  ADD PRIMARY KEY (`id_regadas`),
  ADD KEY `id_bomba` (`cod_bomba`),
  ADD KEY `id_cultivo` (`cod_cultivo`);

--
-- Indexes for table `solo`
--
ALTER TABLE `solo`
  ADD PRIMARY KEY (`id_solo`),
  ADD KEY `id_usuario` (`cod_usuario`);

--
-- Indexes for table `tipo_user`
--
ALTER TABLE `tipo_user`
  ADD PRIMARY KEY (`tipo_user`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `tipo_user` (`cod_tipo_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bombas`
--
ALTER TABLE `bombas`
  MODIFY `id_bomba` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `cultivo`
--
ALTER TABLE `cultivo`
  MODIFY `id_cultivo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `cultura`
--
ALTER TABLE `cultura`
  MODIFY `id_cultura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `leituras`
--
ALTER TABLE `leituras`
  MODIFY `id_leitura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `regadas`
--
ALTER TABLE `regadas`
  MODIFY `id_regadas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `solo`
--
ALTER TABLE `solo`
  MODIFY `id_solo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tipo_user`
--
ALTER TABLE `tipo_user`
  MODIFY `tipo_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `acontecem`
--
ALTER TABLE `acontecem`
  ADD CONSTRAINT `acontecem_ibfk_1` FOREIGN KEY (`id_leitura`) REFERENCES `leituras` (`id_leitura`),
  ADD CONSTRAINT `acontecem_ibfk_2` FOREIGN KEY (`id_cultivo`) REFERENCES `cultivo` (`id_cultivo`);

--
-- Limitadores para a tabela `bombas`
--
ALTER TABLE `bombas`
  ADD CONSTRAINT `bombas_ibfk_1` FOREIGN KEY (`cod_usuario`) REFERENCES `usuario` (`id_usuario`);

--
-- Limitadores para a tabela `cultivo`
--
ALTER TABLE `cultivo`
  ADD CONSTRAINT `cultivo_ibfk_1` FOREIGN KEY (`cod_usuario`) REFERENCES `usuario` (`id_usuario`),
  ADD CONSTRAINT `cultivo_ibfk_2` FOREIGN KEY (`cod_solo`) REFERENCES `solo` (`id_solo`),
  ADD CONSTRAINT `cultivo_ibfk_3` FOREIGN KEY (`cod_cultura`) REFERENCES `cultura` (`id_cultura`);

--
-- Limitadores para a tabela `cultura`
--
ALTER TABLE `cultura`
  ADD CONSTRAINT `cultura_ibfk_1` FOREIGN KEY (`cod_usuario`) REFERENCES `usuario` (`id_usuario`);

--
-- Limitadores para a tabela `leituras`
--
ALTER TABLE `leituras`
  ADD CONSTRAINT `leituras_ibfk_1` FOREIGN KEY (`cod_bomba`) REFERENCES `bombas` (`id_bomba`);

--
-- Limitadores para a tabela `regadas`
--
ALTER TABLE `regadas`
  ADD CONSTRAINT `regadas_ibfk_1` FOREIGN KEY (`cod_bomba`) REFERENCES `bombas` (`id_bomba`),
  ADD CONSTRAINT `regadas_ibfk_2` FOREIGN KEY (`cod_cultivo`) REFERENCES `cultivo` (`id_cultivo`);

--
-- Limitadores para a tabela `solo`
--
ALTER TABLE `solo`
  ADD CONSTRAINT `solo_ibfk_1` FOREIGN KEY (`cod_usuario`) REFERENCES `usuario` (`id_usuario`);

--
-- Limitadores para a tabela `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`cod_tipo_user`) REFERENCES `tipo_user` (`tipo_user`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
