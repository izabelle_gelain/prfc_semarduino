<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Databases
 *
 * @author Iza
 */
class Databases {

   public static function getConnection(){
    
        try{
            $conn= new PDO("mysql:host=localhost;dbname=sies",'root','');
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;
        }  
        catch (PDOException$erro){
            echo 'Conexão falhou: '. $erro->getMessage();
        }
    }
}
