<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Usuario
 *
 * @author Iza
 */
class Usuario {
    private $usuario   ;
    private $senha    ;
    private $nome;
    private $email;
    
    
    public function __construct($usuario, $senha,$nome,$email) {
        $this->usuario = $usuario;
        $this->senha = sha1($senha);
        $this->nome = $nome;
        $this->email = $email;
        
    }

    
   
    function cadastrausuario(){        

        $conn = Databases::getConnection();
        $sql  = "INSERT INTO usuario ( usuario, senha, nome, email, cod_tipo_user) values ('$this->usuario','$this->senha','$this->nome','$this->email', 2)";

        $conn->exec($sql);

        return true;
    }
    

    
    
    public function getUsuario() {
        return $this->usuario;
    }

    public function getSenha() {
        return $this->senha;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getIs_logado() {
        return $this->is_logado;
    }

    public function setUsuario($usuario) {
        $this->usuario = $usuario;
        return $this;
    }

    public function setSenha($senha) {
        $this->senha = $senha;
        return $this;
    }

    public function setNome($nome) {
        $this->nome = $nome;
        return $this;
    }

   
    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }
}
