<html lang="en" class="no-js">
    <head>
        <meta charset="UTF-8">
        <title>SIES - Sistema de Irrigação Eco-Sustentável</title>
         <link rel="shortcut icon" href="img/folha.png">
        <!-- LINKS -->
        <link rel="stylesheet" href="bootstrap-3.3.7/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="fonts/font.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>

    <body>
        <header>
            <!-- NAV -->
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#oi">
                    <span class="fa fa-bars fa-lg"></span>
                </button>
                <div class="container">
                    <a class="navbar-brand" href="index.php">
                        <img src="img/logo.png" alt="" class="logo">
                    </a>
                    <section class="collapse navbar-collapse" id="oi">
                        <ul class="nav navbar-nav navbar-right navbar-collapse collapse in">
                            <li><a href="#sobre">Sobre</a></li>
                            <li><a href="templates/entrar.php">Entrar</a></li>
                        </ul>
                    </section>
                </div>
            </nav>
            <!-- FIM NAV-->
           <article class="container">            
                <section class= "col-md-8 col-md-offset-2">
                    <section class="titulo"> 
                    <p>SIES </p> 
                    </section>
                </section>
            </article>

        </header>


            <!--SOBRE-->
            <section class="sobre" id="sobre">
                <section class="container">
                    <section class="section-heading scrollpoint sp-effect3">
                        <h1>Sobre nós</h1>
                        <div class="divider"></div>
                        <p>BLa bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla blav bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla  bla bla bla bla bla bla bla bla bla bla bla bla bla bla blabla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla</p>
                    </section>
                  
                </section>
            </section>
            <!--FIM SOBRE-->

            <!--FOOTER-->
            <footer>
                <section class="container">
                    <a href="#" class="scrollpoint sp-effect3">
                        <img src="img/logo.png" alt="" class="logo">
                    </a>
                    <div class="rights">
                        <p>&raquo; Izabelle Maria Gelain, Julia Maria Nogueira e Leonardo Vitor Fleith Faria - 3InfoI1 &laquo;</p>       
                    </div>
                </section>
            </footer>
            <!--FIM FOOTER-->
        </div>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/placeholdem.min.js"></script>
        <script src="js/jquery.themepunch.revolution.min.js"></script>
        <script src="js/waypoints.min.js"></script>
        <script src="js/scripts.js"></script>
        <script>
            $(document).ready(function() {
                appMaster.preLoader();
            });
        </script>
    </body>

</html>
