<section class="doublediagonal">
            <div class="container">
            <div class="col-md-8 padding-col">
               <div class="section-heading scrollpoint sp-effect3 dois">
                <h1>Cadastro de Regadas</h1>
            <div class="divider"></div>
            </div>
                <form method="post" action="../../controladores/controlador_cadastro_regadas.php" role="form col-sm-2">
                   <div class="form-group">
                        <select name="id_bomba" class="form-control">
                            <option class=" form-control" value="Título">Nome da Bomba... </option>
                                <?php
                                include_once "../../classes/Arduino.php";
                                $bomba = new Arduino();
                                $bombas = $bomba ->pesquisaBomba ();
                                foreach ($bombas as $bomba) {?>
                                    <option class=" form-control" value="<?=$bomba['id_bomba'];?>"><?=$bomba['nome_bomba'];?></option>
                                 <?php } ?>
                        </select>
                    </div>
                     <div class="form-group">
                        <select name="id_cultivo" class="form-control">
                            <option class=" form-control" value="Título">Nome do Cultivo... </option>
                                <?php
                                include_once "../../classes/Cultivo.php";
                                $cultivo = new Cultivo();
                                $cultivos = $cultivo -> pesquisaCultivo ();
                                foreach ($cultivos as $cultivo) {?>
                                    <option class=" form-control" value="<?=$cultivo['id_cultivo'];?>"><?=$cultivo['nome_cultivo'];?></option>
                                 <?php } ?>
                        </select>
                    </div>
                    <button class="btn btn-primary btn-lg">Enviar</button>       
                </form>   
            </div>
            <div>
        </section>