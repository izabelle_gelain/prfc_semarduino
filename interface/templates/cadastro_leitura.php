<section class="doublediagonal">
    <div class="container">
        <div class="col-md-8 padding-col">
            <div class="section-heading scrollpoint sp-effect3 dois">
                <h1>Cadastro de Leituras</h1>
                <div class="divider"></div>
            </div>

            <form method="GET" action="../../controladores/controlador_cadastro_leitura.php?acao=form" role="form col-sm-2">
                <div class="form-group">
                    <input type="text" name="temperatura" class="form-control" placeholder="Temperatura do Ambiente" value="<?php echo (isset($_SESSION['dados']['temperatura'])? $_SESSION['dados']['temperatura'] : ""); ?>" >

                    <span class="error"><?php echo (isset($_SESSION['erro']['temperatura'])? $_SESSION['erro']['temperatura'] : ""); ?></span>
                </div>

                <div class="form-group">
                    <input type="text" name="umi_ar" class="form-control" placeholder="Umidade do Ar">
                    <span><?php echo (isset($_SESSION['erro']['umi_ar'])? $_SESSION['erro']['umi_ar'] : ""); ?></span>
                </div>

                <div class="form-group">
                    <input type="text" name="umi_solo1" class="form-control" placeholder="Umidade do Solo 1">
                    <span><?php echo (isset($_SESSION['erro']['higrometro1'])? $_SESSION['erro']['umi_solo1'] : ""); ?></span>
                </div>
                       
                        
                <div class="form-group">
                    <select name="id_bomba" class="form-control">
                        <option class=" form-control" value="Título">Nome da Bomba... </option>
                        <?php
                            include_once "../../classes/Arduino.php";
                            $bombaobj = new Arduino();
                            $bombas = $bombaobj -> pesquisaBomba ();
                            foreach ($bombas as $bomba) 
                                {?>
                                    <option class=" form-control" value="<?=$bomba['id_bomba'];?>"><?=$bomba['nome_bomba'];?></option>
                                    <span><?php echo (isset($_SESSION['erro']['id_bomba'])? $_SESSION['erro']['id_bomba'] : ""); ?></span>
                                <?php 
                            } ?>
                    </select>
                </div>
                        
                <button class="btn btn-primary btn-lg">Enviar</button>       
            </form>   
        </div>
    </div>
</section>
               