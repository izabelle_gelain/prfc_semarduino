
<html lang="en" class="no-js">

<head>
    <meta charset="UTF-8">
    <title>SIES - Sistema de Irrigação Eco-Sustentável</title>
    <!-- LINKS -->
    <link rel="stylesheet" href="../bootstrap-3.3.7/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
    <link rel="stylesheet" href="../css/styles.css">
    <!-- LINKS -->
</head>

<body>
    <?php
    require("menu.php") 
    ?>
    <!--ESQUECEU A SENHA-->
    <section id="Entrar" class="doublediagonal">
        <div class="container">
            <div class="section-heading scrollpoint sp-effect3">
                <h1>Esqueceu sua senha?</h1>
                <div class="divider"></div>
            </div>
            <div class="row">
                <div class="col-md-6 ">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-8 col-sm-7 col-sm-offset-3 col-xs-8 col-xs-offset-2  scrollpoint sp-effect1 ">
                            <form role="form">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Email">
                                </div>
                                <a href="dashboard.php" class="btn btn-primary btn-lg">Enviar</a>  
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--FIM ESQUECEU SENHA --> 
    <?php
        require("footer.php") 
    ?>

    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
    <script src="../js/slick.min.js"></script>
    <script src="../js/placeholdem.min.js"></script>
 
    <script src="../js/jquery.themepunch.revolution.min.js"></script>
    <script src="../js/waypoints.min.js"></script>
    <script src="../js/scripts.js"></script>
    <script>
        $(document).ready(function() {
            appMaster.preLoader();
        });
    </script>
</body>
</html>
