
<html lang="en" class="no-js">

<head>
    <meta charset="UTF-8">
    <title>SIES - Sistema de Irrigação Eco-Sustentável</title>
    <!-- LINKS -->
    <link rel="stylesheet" href="../bootstrap-3.3.7/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
      <link rel="stylesheet" href="../fonts/font.css">
    <link rel="stylesheet" href="../css/styles.css">
    <!-- LINKS -->
</head>

<body>
    <?php
        require("menu.php") 
    ?>
    <!--ENTRAR-->
    <section id="Entrar" class="doublediagonal">
        <div class="container">
            <div class="section-heading scrollpoint sp-effect3">
                <h1>Entrar</h1>
            <div class="divider"></div>
            </div>
            <div class="row">
                <div class="col-md-6 ">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-8 col-sm-7 col-sm-offset-3 col-xs-8 col-xs-offset-2  scrollpoint sp-effect1 ">
                            <form role="form" action="../../controladores/controlador_entrar.php" method="post">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Usuário" name="usuario">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Senha" name="senha">
                                </div>
                                <div class="media">
                                    <div class="media-body">
                                        <h4 class="media-heading">
                                            <a href="esqueceusenha.php">Esqueceu a senha?</a>
                                             <button type="submit" class="btn btn-primary btn-lg">Entrar</button>
                                        </h4>
                                    </div>
                                </div>
                                    <div class="media">
                                        <div class="media-body">
                                            <h4 class="media-heading">Ainda não é cadastrado?</h4>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading">
                                                <a href="cadastrousuario.php" >  Cadastre-se</a>
                                            </h4>
                                        </div>
                                    </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--FIM ENTRAR --> 
    <?php
        require("footer.php") 
    ?>

    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
    <script src="../js/slick.min.js"></script>
    <script src="../js/placeholdem.min.js"></script>
 
  
    <script src="../js/waypoints.min.js"></script>
    <script src="../js/scripts.js"></script>
    <script>
        $(document).ready(function() {
            appMaster.preLoader();
        });
    </script>
</body>
</html>
