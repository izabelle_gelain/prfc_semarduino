<!--FOOTER-->
        <footer>
            <div class="container">
                <a href="#" class="scrollpoint sp-effect3">
                    <img src="../img/logo.png" alt="" class="logo">
                </a>
                <div class="social">
                    <a href="#" class="scrollpoint sp-effect3"><i class="fa fa-twitter fa-lg"></i></a>
                    <a href="#" class="scrollpoint sp-effect3"><i class="fa fa-google-plus fa-lg"></i></a>
                    <a href="#" class="scrollpoint sp-effect3"><i class="fa fa-facebook fa-lg"></i></a>
                </div>
                <div class="rights">
                    <p>&raquo; Izabelle Maria Gelain, Julia Maria Nogueira e Leonardo Vitor Fleith Faria - 3InfoI1 &laquo;</p>       
                </div>
            </div>
        </footer>
<!--FIM FOOTER-->