 
<section class="doublediagonal">
            <div class="container">
            <div class="col-md-8 padding-col">
               <div class="section-heading scrollpoint sp-effect3 dois">
                <h1>Cadastro de Cultivo</h1>
            <div class="divider"></div>
            </div>
                <form method="post" action="../../controladores/controlador_cadastro_cultivo.php" role="form col-sm-2">
                <div class="form-group">
                        <input type="text" name="nome_cultivo" class="form-control" placeholder="Nome Cultivo">
                    </div>
                    <div class="form-group">
                         <select name="id_cultura" class="form-control">
                            <option class=" form-control" value="Título">Nome Cultura...</option>  
                                <?php
                                include_once "../../classes/Cultura.php";
                                $tipo_planta = new Cultura();
                                $tipo_plantas = $tipo_planta -> pesquisaCultura();
                                foreach ($tipo_plantas as $tipo_planta) {?>  
                                    <option class=" form-control" value="<?= $tipo_planta['id_cultura'];?>"><?=$tipo_planta['tipo_planta']; ?></option>
                                   
                                 <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="id_solo" class="form-control">
                            <option class=" form-control" value="Título">Nome da Solo... </option>
                                <?php
                                include_once "../../classes/Solo.php";
                                $tipo_solo = new Solo();
                                $tipo_solos = $tipo_solo -> pesquisaSolo ();
                                foreach ($tipo_solos as $tipo_solo) {?>
                                    <option class=" form-control" value="<?=$tipo_solo['id_solo'];?>"><?=$tipo_solo['tipo_solo'];?></option>
                                 <?php } ?>
                        </select>
                    </div>
                        <?php
                           
                            $id_user = $_SESSION['logar']['id_user'];
                            echo ('
                              
                                <input type="hidden" name="id_user" value="'.$id_user.'" class="form-control">
                            '); 
                        ?>
                 
                 <button class="btn btn-primary btn-lg">Enviar</button>       
                </form>   
            </div>
            <div>
        </section>