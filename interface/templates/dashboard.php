<?php
include "../../classes/Login.php";

$logado = new Login();
  
$logado -> ta_Logado();

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Bem Vindo ao SIES!</title>
    <!-- LINKS -->
    <link href="../bootstrap-3.3.7/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../css/light-bootstrap-dashboard.css" rel="stylesheet"/>
    <link rel="stylesheet" href="../css/animate.css">
    <link rel="stylesheet" href="../css/font-awesome.min.css">
      <link rel="stylesheet" href="../fonts/font.css">
    <link rel="stylesheet" href="../css/styles.css">
    <!-- LINKS -->
</head>

<body>
    <div class="wrapper">
        
        <?php
     include_once "classes/login.php";

     $login = new Login();

    $usuario=$_SESSION['logar']['nome'];
    
     $teste = $login->Logado();


            $menu = [
            'menu1'=>'dashboard.php?pos=1&pgs=cadastro_cultura.php',
            'menu2'=>'dashboard.php?pos=1&pgs=cadastro_solo.php',
            'menu3'=>'dashboard.php?pos=1&pgs=cadastro_cultivo.php',
            'menu4'=>'dashboard.php?pos=1&pgs=cadastro_bombas.php',
            'menu5'=>'dashboard.php?pos=1&pgs=cadastro_leitura.php',
            'menu6'=>'dashboard.php?pos=1&pgs=cadastro_regadas.php',
            'menu7'=>'',
            'menu8'=>'dashboard.php?pos=1&pgs=cadastrados_cultivo.php'];
            if ($teste==true) {
  
            $template1 = getTemplate('templates/menu_logado.php');
            $templateFinal = parseTemplate( $template1, $menu );
            echo $templateFinal;}
            else {
              $template1 = getTemplate('templates/menu.php');
            $templateFinal = parseTemplate( $template1, $menu );
            echo $templateFinal;
            }
      ?>
      
<div class="main-panel">
    <nav class="navbar navbar-default navbar-fixed">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Olá <?php
                        $nome = $_SESSION['logar']['nome'];
                        echo $nome;
                        ?></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        
                        <a href="../../controladores/controlador_saida.php">
                            <p>Log out</p>  

                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav> 
    <?php
    include_once '../../controladores/controlador.php';
    ins_dados(filter_input(INPUT_GET,'pos'), filter_input(INPUT_GET, 'pgs'));
    ?> 

</div>
</div>
</body>

<script src="../js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="../js/light-bootstrap-dashboard.js"></script>

<script src="../js/jquery-1.11.1.min.js"></script>
<script src="../bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
<script src="../js/slick.min.js"></script>
<script src="../js/placeholdem.min.js"></script>

<script src="../js/jquery.themepunch.revolution.min.js"></script>
<script src="../js/waypoints.min.js"></script>
<script src="../js/scripts.js"></script>
</html>
