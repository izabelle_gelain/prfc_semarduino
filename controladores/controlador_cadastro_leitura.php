<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

date_default_timezone_set('America/Sao_Paulo');
session_start();

include_once "../classes/Leitura.php";

$cod_arduino = $_GET['idcontrolador'];

if ($cod_arduino != null) {

if(isset($_GET['temperatura']) && isset($_GET['umidade']) && isset($_GET['higrometro1'])){

	unset($_SESSION['erro']);
	unset($_SESSION['dados ']);
	$erro = false;
	

	//valida temperatura
	$temperatura = filter_input(INPUT_GET, 'temperatura');

		if ($temperatura == false) {
			$erro = true;
			$_SESSION['erro']['temperatura'] = "Temperatura está incorreta";
		} else {
			$_SESSION['dados']['temperatura'] = $temperatura;
		}

	//valida umidade do ar
	$umi_ar = filter_input(INPUT_GET, 'umidade');

		if ($umi_ar == false) {
			$erro = true;
			$_SESSION['erro']['umi_ar'] = "Umidade do ar está errada";	
		}else {
			$_SESSION['dados']['umi_ar'] = $umi_ar;
		}

	$umi_solo1 = filter_input(INPUT_GET, 'higrometro1');

		if ($umi_ar == false) {
			$erro = true;
			$_SESSION['erro']['umi_solo1'] = "Umidade do solo errada";
		}else {
			$_SESSION['dados']['umi_solo1'] = $umi_solo1;
		}


	$id_bomba = filter_input(INPUT_GET,'id_bomba', FILTER_VALIDATE_INT);

	
	$data = date('m.d.Y');
	$hora = date('H:i:s');


	if ($erro == false) {
		

		$cadastrar = new Leitura();
        
		$cadastrar -> cadastraLeitura($temperatura, $data, $umi_ar, $higrometro1, $hora, $cod_arduino);

		
	}

	}
}



