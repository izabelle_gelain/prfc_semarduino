<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include_once "../classes/Login.php";


$usuario = filter_input(INPUT_POST,'usuario');
$senha = filter_input(INPUT_POST,'senha');


$logar = new Login();

$logar->efetuaLogin($usuario, $senha);

$logando = $logar->ta_Logado();

if ($logando == true) {

	header("location:../interface/templates/dashboard.php");
	
}else{
	header("location:../interface/templates/entrar.php");
}
