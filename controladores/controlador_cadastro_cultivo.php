<?php

include_once "../classes/Cultivo.php";


$id_solo = filter_input(INPUT_POST,'id_solo');
$id_cultura = filter_input(INPUT_POST,'id_cultura');
$nome_cultivo = filter_input(INPUT_POST,'nome_cultivo');
$id_user = filter_input(INPUT_POST,'id_user');


$cadastrar = new Cultivo();
        
$cadastrar -> cadastraCultivo($nome_cultivo, $id_user, $id_cultura, $id_solo);


header("location:../interface/templates/dashboard.php?pos=1&pgs=cadastro_cultivo.php");

