
<?php

require_once '../classes/Usuario.php';
require_once '../classes/Login.php';

$nome = filter_input(INPUT_POST,'nome');
$usuario = filter_input(INPUT_POST,'usuario');
$email = filter_input(INPUT_POST,'email');
$senha = filter_input(INPUT_POST,'senha');
$confirmar_senha = filter_input(INPUT_POST,'confirmar_senha');

if ($nome != false && $usuario != false && $email != false && $senha != false){

    if ($senha == $confirmar_senha){

        $conexao = Databases::getConnection();

        /*Verifica se já há um usuário cadastrado com este login*/
        $verificacao = "SELECT * FROM usuario WHERE usuario = '$usuario' AND email ='$email' ";
        $retorna = $conexao->query($verificacao);
        $verificacao = $retorna->fetch(PDO::FETCH_ASSOC);

        /*Cadastra as informações do novo usuário no banco*/
        if ($verificacao == false)
        {

            $cadastrar =  new Usuario($usuario,$senha,$nome,$email);
            $cadastrar -> cadastrausuario($usuario,$senha,$nome,$email);

            if($cadastrar == true){
                /*Faz o login*/
                $logar = new Login();
                $logar->efetuaLogin($usuario, $senha);

                echo("<script type='text/javascript'> alert( 'Parabéns, seu usuário foi cadastrado com sucesso! :)' );
                  location.href='../interface/templates/dashboard.php';</script>");}

            }else{
                echo("<script type='text/javascript'> alert( 'Já existe alguém cadastrado com este usuario ou email! Tente novamente :)' );
                  location.href='../interface/templates/cadastro_usuario.php';</script>");
            }

        }else{
          echo("<script type='text/javascript'> alert( 'Sua senha não está igual à confirmação! Tente novamente :)' );
            location.href='../interface/templates/cadastro_usuario.php';</script>");
      }

    }else{
      echo("<script type='text/javascript'> alert( 'Você deixou campos vazios! Tente novamente :)' );
      location.href='../interface/templates/cadastro_usuario.php';</script>");
}