<?php

include_once "../classes/Solo.php";
include_once "controlador.php";

$tipo_solo = filter_input(INPUT_POST,'tipo_solo');
$cap_campo = filter_input(INPUT_POST,'cap_campo');
$tipo_user = filter_input(INPUT_POST,'tipo_user');
$id_user = filter_input(INPUT_POST,'id_user');
$status = status($tipo_user);

$cadastrar = new Solo();
        
$cadastrar -> cadastrasolo($tipo_solo,$cap_campo,$id_user,$status);
						   
if ($tipo_user == 1) {
	header("location:../interface/templates/dashboard.php?pos=1&pgs=cadastro_solo_Admin.php");
}
else{

	header("location:../interface/templates/dashboard.php?pos=1&pgs=cadastro_solo.php");
}


